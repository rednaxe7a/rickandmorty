# RICK AND MORTY
### Author: Alexander Piñero
### Email: rednaxe7@gmail.com

## Build and execution instructions

1. IDE Recomendado IntelliJ IDEA para abrir el proyecto con Java 11
2. Dejar que automaticamente se empiecen a descargar las dependencias
3. Dirigirse a el panel de Gradle, application bootRun para correr la aplicación
4. La aplicación estara corriendo sobre el puerto 8080, verificar que no este tomado por otro proceso, o cambiar el puerto en el archivo application.properties
5. La url para probar seria
   http://localhost:8080/rickandmorty/getCharacter/1

## Bye!