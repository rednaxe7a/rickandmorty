package com.mobdev.rickandmorty.service.impl;

import com.mobdev.rickandmorty.domain.*;
import com.mobdev.rickandmorty.util.ApiCall;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.*;
import java.util.ArrayList;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class CharacterAImplTests {

    @InjectMocks
    CharacterAImpl service;

    @Mock
    ApiCall apiCall;


    /*@Mock
    FunctionCharacterAFromB f;*/

    CharacterB characterB = new CharacterB();
    CharacterBLocation characterBLocation = new CharacterBLocation();
    LocationB locationB = new LocationB();

    @BeforeEach
    void setUp() throws Exception {
        MockitoAnnotations.openMocks(this);
        //f.characterOriginalToA = mock(Function.class);
        //f.locationOriginalToCharacterAOrigin= mock(Function.class);

        ArrayList<String> list = new ArrayList<>();
        list.add("a");

        characterBLocation.setName("");
        characterBLocation.setUrl("");
        characterBLocation.getName();
        characterBLocation.getUrl();

        characterB.setId(1);
        characterB.setName("Rick");
        characterB.setStatus("Alive");
        characterB.setSpecies("species");
        characterB.setType("type");
        characterB.setGender("Male");
        characterB.setOrigin(characterBLocation);
        characterB.setLocation(characterBLocation);
        characterB.setImage("");
        characterB.setEpisode(list);
        characterB.setUrl("test");
        characterB.setCreated("");

        characterB.getId();
        characterB.getName();
        characterB.getStatus();
        characterB.getSpecies();
        characterB.getType();
        characterB.getGender();
        characterB.getOrigin();
        System.out.println(characterB.getOrigin().getUrl());
        characterB.getLocation();
        characterB.getImage();
        characterB.getEpisode();
        characterB.getUrl();
        characterB.getCreated();

        locationB.setId(1);
        locationB.setName("");
        locationB.setType("");
        locationB.setDimension("");
        locationB.setResidents(null);
        locationB.setUrl("");
        locationB.setCreated("");

        locationB.getId();
        locationB.getName();
        locationB.getType();
        locationB.getDimension();
        locationB.getResidents();
        locationB.getUrl();
        locationB.getCreated();

    }

    @Test
    public void getCharacterTest() {

        //Mockito.when(apiCall.getCharacterB(ArgumentMatchers.anyString())).thenReturn(characterB);

        //Mockito.when(apiCall.get(ArgumentMatchers.any(Class.class), ArgumentMatchers.anyString())).thenReturn(characterB);
        //Mockito.when(apiCall.get(ArgumentMatchers.any(Class.class), ArgumentMatchers.anyString())).thenReturn(locationB);

        Mockito.when(apiCall.get(ArgumentMatchers.eq(CharacterB.class), ArgumentMatchers.anyString())).thenReturn(characterB);
        Mockito.when(apiCall.get(ArgumentMatchers.eq(LocationB.class), ArgumentMatchers.anyString())).thenReturn(locationB);

        assertNotNull(service.getCharacter(1),"getCharacter CharacterAImpl");
    }

}
