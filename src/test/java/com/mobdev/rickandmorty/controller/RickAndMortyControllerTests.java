package com.mobdev.rickandmorty.controller;

import com.mobdev.rickandmorty.domain.CharacterA;
import com.mobdev.rickandmorty.domain.CharacterAOrigin;
import com.mobdev.rickandmorty.service.ICharacter;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;


import static org.junit.jupiter.api.Assertions.assertEquals;

public class RickAndMortyControllerTests {

    @InjectMocks
    RickAndMortyController controller;

    @Mock
    ICharacter service;

    CharacterA characterA = new CharacterA();
    CharacterAOrigin characterAOrigin = new CharacterAOrigin();

    @BeforeEach
    void setUp() throws Exception {
        MockitoAnnotations.openMocks(this);

        characterAOrigin.setName("");
        characterAOrigin.setDimension("");
        characterAOrigin.setUrl("");
        characterAOrigin.setResidents(null);

        characterAOrigin.getName();
        characterAOrigin.getDimension();
        characterAOrigin.getUrl();
        characterAOrigin.getResidents();


        characterA.setId(1);
        characterA.setName("Rick");
        characterA.setStatus("Alive");
        characterA.setSpecies("Human");
        characterA.setType("");
        characterA.setEpisode_count(51);
        characterA.setOrigin(characterAOrigin);

        characterA.getId();
        characterA.getName();
        characterA.getStatus();
        characterA.getSpecies();
        characterA.getType();
        characterA.getEpisode_count();
        characterA.getOrigin();

    }

    @Test
    public void pingTest() {
        ResponseEntity re = new ResponseEntity<>("OK", HttpStatus.OK);
        assertEquals(HttpStatus.OK, controller.ping().getStatusCode(),"ping controller");
    }

    @Test
    public void getCharacterTest() {

        ResponseEntity re = new ResponseEntity<>(characterA, HttpStatus.OK);
        Mockito.when(service.getCharacter(1)).thenReturn(re);
        assertEquals(HttpStatus.OK, controller.getCharacter(1).getStatusCode(),"getCharacter controller");
    }





}
