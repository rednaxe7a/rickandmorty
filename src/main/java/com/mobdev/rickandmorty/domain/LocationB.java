package com.mobdev.rickandmorty.domain;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
@Data
@Getter
@Setter
public class LocationB {
    private int id;
    private String name;
    private String type;
    private String dimension;
    private ArrayList<String> residents = new ArrayList<String>();
    private String url;
    private String created;
}
