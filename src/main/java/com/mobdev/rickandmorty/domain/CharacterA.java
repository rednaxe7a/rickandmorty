package com.mobdev.rickandmorty.domain;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Data
@Getter
@Setter
public class CharacterA {

    private int id;
    private String name;
    private String status;
    private String species;
    private String type;
    private int episode_count;
    private CharacterAOrigin origin;


}
