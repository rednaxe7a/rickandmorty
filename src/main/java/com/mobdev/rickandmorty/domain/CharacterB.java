package com.mobdev.rickandmorty.domain;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;

@Data
@Getter
@Setter
public class CharacterB {
    private int id;
    private String name;
    private String status;
    private String species;
    private String type;
    private String gender;
    CharacterBLocation origin;
    CharacterBLocation location;
    private String image;
    private ArrayList<String> episode = new ArrayList<>();
    private String url;
    private String created;
}
