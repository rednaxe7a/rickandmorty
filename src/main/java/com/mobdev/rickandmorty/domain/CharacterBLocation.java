package com.mobdev.rickandmorty.domain;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Data
@Getter
@Setter
public class CharacterBLocation {
    private String name;
    private String url;
}
