package com.mobdev.rickandmorty.domain;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import java.util.ArrayList;

@Data
@Getter
@Setter
public class CharacterAOrigin {

    @NotNull(message = "name is null Field required")
    private String name;
    @NotNull(message = "url is null Field required")
    private String url;
    @NotNull(message = "dimension is null Field required")
    private String dimension;
    @NotNull(message = "residents is null. Field required")
    private ArrayList<String> residents = new ArrayList<>();

}
