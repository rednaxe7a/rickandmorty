package com.mobdev.rickandmorty.service.impl;

import com.mobdev.rickandmorty.domain.*;
import com.mobdev.rickandmorty.service.ICharacter;
import com.mobdev.rickandmorty.util.ApiCall;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;


import java.util.function.Function;

@Service
public class CharacterAImpl implements ICharacter
{
    @Autowired
    ApiCall apiCall;

    @Value("${rickandmorty.url.character}")
    String urlCharacter;

    Function<CharacterB, CharacterA> characterOriginalToA
            = new Function<CharacterB, CharacterA>() {

        public CharacterA apply(CharacterB t) {
            CharacterA characterA = new CharacterA();
            characterA.setId(t.getId());
            characterA.setName(t.getName());
            characterA.setStatus(t.getStatus());
            characterA.setSpecies(t.getSpecies());
            characterA.setType(t.getType());
            characterA.setEpisode_count(t.getEpisode().size());
            return characterA;
        }
    };

    Function<LocationB, CharacterAOrigin> locationOriginalToCharacterAOrigin
            = new Function<LocationB, CharacterAOrigin>() {

        public CharacterAOrigin apply(LocationB t) {
            CharacterAOrigin characterAOrigin = new CharacterAOrigin();
            characterAOrigin.setName(t.getName());
            characterAOrigin.setUrl(t.getUrl());
            characterAOrigin.setDimension(t.getDimension());
            characterAOrigin.setResidents(t.getResidents());
            return characterAOrigin;
        }
    };

    @Override
    public CharacterA getCharacter(int id) {
        //CharacterB characterOriginal = apiCall.getCharacterB(urlCharacter + id );
        CharacterB characterOriginal = (CharacterB) apiCall.get(CharacterB.class,urlCharacter + id );
        LocationB locationOriginal = (LocationB) apiCall.get(LocationB.class,characterOriginal.getOrigin().getUrl());

        CharacterA characterA = characterOriginalToA.apply(characterOriginal);
        if (locationOriginal != null){
            CharacterAOrigin characterAOrigin = locationOriginalToCharacterAOrigin.apply(locationOriginal);
            characterA.setOrigin(characterAOrigin);
        }
        return characterA;
    }

}
