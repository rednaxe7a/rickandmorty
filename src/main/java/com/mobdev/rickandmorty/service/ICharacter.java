package com.mobdev.rickandmorty.service;

import com.mobdev.rickandmorty.domain.CharacterA;

public interface ICharacter<T> {
    T getCharacter(int id);
}
