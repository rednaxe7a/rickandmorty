package com.mobdev.rickandmorty.util;

import com.mobdev.rickandmorty.domain.CharacterB;
import com.mobdev.rickandmorty.domain.LocationB;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
public class ApiCall<T>{

    @Autowired
    RestTemplate restTemplate;

    public T get(Class<T> c,String url) {
        if (url.isEmpty()){
            return null;
        }
        ResponseEntity<T> responseResponseEntity = restTemplate
                .getForEntity(url, c);
        return responseResponseEntity.getBody();
    }

/*    public CharacterB getCharacterB(String url) {
        if (url.isEmpty()){
            return null;
        }
        ResponseEntity<CharacterB> responseResponseEntity = restTemplate
                .getForEntity(url, CharacterB.class);
        return responseResponseEntity.getBody();
    }*/

}
