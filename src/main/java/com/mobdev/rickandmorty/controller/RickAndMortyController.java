package com.mobdev.rickandmorty.controller;

import com.mobdev.rickandmorty.service.ICharacter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Slf4j
public class RickAndMortyController<T> {
    @Autowired
    ICharacter service;

    @GetMapping(value = "/ping", produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<String> ping() {
        return new ResponseEntity<>("OK", HttpStatus.OK);
    }

    @GetMapping(value = "/getCharacter/{id}", produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<T> getCharacter(@PathVariable("id") int id) {
        return new ResponseEntity<>((T) service.getCharacter(id), HttpStatus.OK);
    }

}
